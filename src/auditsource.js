'use strict';

exports = module.exports = {
    CRON:  { userId: null, username: 'cron' },
    HEALTH_MONITOR: { userId: null, username: 'healthmonitor' },
    APP_TASK: { userId: null, username: 'apptask' },
    EXTERNAL_LDAP_TASK: { userId: null, username: 'externalldap' },
    EXTERNAL_LDAP_AUTO_CREATE: { userId: null, username: 'externalldap' },

    fromRequest: fromRequest
};

function fromRequest(req) {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || null;
    return { ip: ip, username: req.user ? req.user.username : null, userId: req.user ? req.user.id : null };
}
