'use strict';

exports = module.exports = {
    get: get,
    update: update,
    getAvatar: getAvatar,
    setAvatar: setAvatar,
    clearAvatar: clearAvatar,
    changePassword: changePassword,
    setTwoFactorAuthenticationSecret: setTwoFactorAuthenticationSecret,
    enableTwoFactorAuthentication: enableTwoFactorAuthentication,
    disableTwoFactorAuthentication: disableTwoFactorAuthentication
};

var assert = require('assert'),
    auditSource = require('../auditsource.js'),
    BoxError = require('../boxerror.js'),
    fs = require('fs'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    path = require('path'),
    paths = require('../paths.js'),
    safe = require('safetydance'),
    users = require('../users.js'),
    settings = require('../settings.js'),
    _ = require('underscore');

function get(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    const emailHash = require('crypto').createHash('md5').update(req.user.email).digest('hex');

    next(new HttpSuccess(200, {
        id: req.user.id,
        username: req.user.username,
        email: req.user.email,
        fallbackEmail: req.user.fallbackEmail,
        displayName: req.user.displayName,
        twoFactorAuthenticationEnabled: req.user.twoFactorAuthenticationEnabled,
        role: req.user.role,
        source: req.user.source,
        avatarUrl: fs.existsSync(path.join(paths.PROFILE_ICONS_DIR, req.user.id)) ? `${settings.adminOrigin()}/api/v1/profile/avatar/${req.user.id}` : `https://www.gravatar.com/avatar/${emailHash}.jpg`
    }));
}

function update(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');
    assert.strictEqual(typeof req.body, 'object');

    if ('email' in req.body && typeof req.body.email !== 'string') return next(new HttpError(400, 'email must be string'));
    if ('fallbackEmail' in req.body && typeof req.body.fallbackEmail !== 'string') return next(new HttpError(400, 'fallbackEmail must be string'));
    if ('displayName' in req.body && typeof req.body.displayName !== 'string') return next(new HttpError(400, 'displayName must be string'));

    var data = _.pick(req.body, 'email', 'fallbackEmail', 'displayName');

    users.update(req.user, data, auditSource.fromRequest(req), function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}

function setAvatar(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    if (!req.files.avatar) return next(new HttpError(400, 'avatar is missing'));

    if (!safe.fs.renameSync(req.files.avatar.path, path.join(paths.PROFILE_ICONS_DIR, req.user.id))) return next(new HttpError(500, safe.error));

    next(new HttpSuccess(202, {}));
}

function clearAvatar(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    safe.fs.unlinkSync(path.join(paths.PROFILE_ICONS_DIR, req.user.id));

    next(new HttpSuccess(202, {}));
}

function getAvatar(req, res) {
    res.sendFile(path.join(paths.PROFILE_ICONS_DIR, req.params.identifier));
}

function changePassword(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');
    assert.strictEqual(typeof req.user, 'object');

    if (typeof req.body.newPassword !== 'string') return next(new HttpError(400, 'newPassword must be a string'));

    users.setPassword(req.user, req.body.newPassword, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(204));
    });
}

function setTwoFactorAuthenticationSecret(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    users.setTwoFactorAuthenticationSecret(req.user.id, function (error, result) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(201, { secret: result.secret, qrcode: result.qrcode }));
    });
}

function enableTwoFactorAuthentication(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');
    assert.strictEqual(typeof req.user, 'object');

    if (!req.body.totpToken || typeof req.body.totpToken !== 'string') return next(new HttpError(400, 'totpToken must be a nonempty string'));

    users.enableTwoFactorAuthentication(req.user.id, req.body.totpToken, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202, {}));
    });
}

function disableTwoFactorAuthentication(req, res, next) {
    assert.strictEqual(typeof req.user, 'object');

    users.disableTwoFactorAuthentication(req.user.id, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(202, {}));
    });
}
