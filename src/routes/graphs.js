'use strict';

exports = module.exports = {
    getGraphs: getGraphs
};

var middleware = require('../middleware/index.js'),
    HttpError = require('connect-lastmile').HttpError,
    url = require('url');

// for testing locally: curl 'http://127.0.0.1:8417/graphite-web/render?format=json&from=-1min&target=absolute(collectd.localhost.du-docker.capacity-usage)'
// the datapoint is (value, timestamp) https://buildmedia.readthedocs.org/media/pdf/graphite/0.9.16/graphite.pdf
const graphiteProxy = middleware.proxy(url.parse('http://127.0.0.1:8417'));

function getGraphs(req, res, next) {
    var parsedUrl = url.parse(req.url, true /* parseQueryString */);
    delete parsedUrl.query['access_token'];
    delete req.headers['authorization'];
    delete req.headers['cookies'];

    // 'graphite-web' is the URL_PREFIX in docker-graphite
    req.url = url.format({ pathname: 'graphite-web/render', query: parsedUrl.query });

    // graphs may take very long to respond so we run into headers already sent issues quite often
    // nginx still has a request timeout which can deal with this then.
    req.clearTimeout();

    graphiteProxy(req, res, function (error) {
        if (!error) return next();

        if (error.code === 'ECONNREFUSED') return next(new HttpError(424, 'Unable to connect to graphite'));
        // ECONNRESET here is most likely because of a bug in the query or the uwsgi buffer size is too small
        if (error.code === 'ECONNRESET') return next(new HttpError(424, 'Unable to query graphite'));

        next(new HttpError(500, error));
    });
}

