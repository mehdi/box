'use strict';

const crypto = require('crypto');

exports.up = function(db, callback) {
    db.all('SELECT value FROM settings WHERE name="backup_config"', function (error, results) {
        if (error || results.length === 0) return callback(error);

        var backupConfig = JSON.parse(results[0].value);
        if (backupConfig.key) {
            const aesKeys = crypto.scryptSync(backupConfig.key, Buffer.from('CLOUDRONSCRYPTSALT', 'utf8'), 128);
            backupConfig.encryption = {
                dataKey: aesKeys.slice(0, 32).toString('hex'),
                dataHmacKey: aesKeys.slice(32, 64).toString('hex'),
                filenameKey: aesKeys.slice(64, 96).toString('hex'),
                filenameHmacKey: aesKeys.slice(96).toString('hex')
            };
        } else {
            backupConfig.encryption = null;
        }

        delete backupConfig.key;

        db.runSql('UPDATE settings SET value=? WHERE name="backup_config"', [ JSON.stringify(backupConfig) ], callback);
    });
};

exports.down = function(db, callback) {
  callback();
};
