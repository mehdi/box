'use strict';

var fs = require('fs');

exports.up = function(db, callback) {
    let sysinfoConfig = { provider: 'generic' };

    db.runSql('REPLACE INTO settings (name, value) VALUES(?, ?)', [ 'sysinfo_config', JSON.stringify(sysinfoConfig) ], callback);
};

exports.down = function(db, callback) {
    callback();
};
