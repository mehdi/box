'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE apps CHANGE installationProgress errorJson TEXT', [], function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE apps CHANGE errorJson installationProgress TEXT', [], function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
