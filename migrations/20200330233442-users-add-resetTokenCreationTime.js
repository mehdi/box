'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE users ADD COLUMN resetTokenCreationTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE users DROP COLUMN resetTokenCreationTime', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
